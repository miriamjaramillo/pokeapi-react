import { BrowserRouter,
    Switch,
    Route} from 'react-router-dom'
import Pokedex from './containers/pokedex';
import PokeDetalle from './components/PokeDetalle'
import TermsAndConditions from "./pages/TermsAndConditions";
import Home from "./pages/Home";
import NotFoundPage from './pages/NotFoundPage'

const Router = () => {
  return (
    <BrowserRouter>
        <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/generacion/1">
              <Pokedex generation="pokemon?limit=151" />
            </Route>
            <Route exact path="/generacion/2">
              <Pokedex generation="pokemon?limit=100&offset=151" />
            </Route>
            <Route exact path="/generacion/3">
              <Pokedex generation="pokemon?limit=135&offset=250" />
            </Route>
            <Route exact path="/generacion/4">
              <Pokedex generation="pokemon?limit=107&offset=386" />
            </Route>
            <Route exact path="/generacion/5">
              <Pokedex generation="pokemon?limit=156&offset=493" />
            </Route>
            <Route exact path="/generacion/6">
              <Pokedex generation="pokemon?limit=72&offset=649" />
            </Route>
            <Route exact path="/generacion/7">
              <Pokedex generation="pokemon?limit=86&offset=721" />
            </Route>
            <Route exact path="/pokemon/:name">
              <PokeDetalle />
            </Route>
            <Route path="/terms-conditions">
              <TermsAndConditions />
            </Route>

            <Route>
              <NotFoundPage />
            </Route>
        </Switch>
    </BrowserRouter>
  );
};

export default Router;
