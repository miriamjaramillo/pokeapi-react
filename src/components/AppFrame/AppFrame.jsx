import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import ErrorBoundary from './../../generic/ErrorBoundary'
import Header from './../Header'
import Footer from './../Footer'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    footer: {
        position: 'fixed',
        bottom: 0,
        width: '100%'
    },
});

const AppFrame = ({ children }) => {
    const classes = useStyles();
    return (
        <Grid container>
            <Grid item xs={12}>
                <Header />
            </Grid>
            <Grid item xs={12}>
                <ErrorBoundary>{children}</ErrorBoundary>
            </Grid>
            <Grid item xs={12} className={classes.footer}>
                <Footer />
            </Grid>
        </Grid>
    )
}

AppFrame.propTypes = {
    children: PropTypes.node
}

export default AppFrame