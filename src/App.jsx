import React from 'react'
import Router from "./Router";
import { Container } from "@material-ui/core";
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';

const App = () => {
    return (
      <ThemeProvider theme={theme}>
        <Container>
          <Router />
        </Container>
      </ThemeProvider>
      
    )
}

export default App
