import React from 'react'
import { Link } from 'react-router-dom';
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction  from '@material-ui/core/BottomNavigationAction'
import RestoreIcon from '@material-ui/icons/Restore'
import BookIcon from '@material-ui/icons/Book'

const Footer = () => {
    return(
        <BottomNavigation showLabels>
            <BottomNavigationAction component={Link}
                                    to="/"
                                    label="Home" icon = {<RestoreIcon />}/>
            <BottomNavigationAction component={Link}
                                    to="/terms-conditions"
                                    label="Términos" icon = {<BookIcon />}/>
        </BottomNavigation>
    )
}
export default Footer;