import { combineReducers } from 'redux';
import boolean from './booleanHome';
import details from './showDetails';
import pokemonReducer from './pokemones';

const rootReducer = combineReducers({
  isInHome: boolean,
  details,
  pokemones: pokemonReducer,
});

export default rootReducer;
