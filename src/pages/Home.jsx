import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardMedia from '@material-ui/core/CardMedia'
import { IconButton, CardActions } from '@material-ui/core'
import ImageList from '@material-ui/core/ImageList'
import ImageListItem from '@material-ui/core/ImageListItem'
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid'
import ArrowCircleRightIcon from '@material-ui/icons/ForwardTwoTone';
import AppFrame from './../components/AppFrame'
import { boolHome, cleanInput } from '../actions/index';

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        const { setBoolHome, clearInput } = props;
        setBoolHome(true);
        clearInput();
      }
      render() {
        return (
            <AppFrame>
                <Grid container spacing={2}>
                    <Grid item md={4}>
                        <Card>
                            <CardHeader
                                avatar={
                                    <Avatar>1ra</Avatar>
                                }
                                title="Primera Generación"
                                subheader="27 de febrero de 1996"
                            />
                            <CardMedia>
                                <ImageList cols={3}>
                                    {imgPrimeraGen.map((item) => (
                                        <ImageListItem key={item.img}>
                                            <img
                                                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                                alt={item.title}
                                                loading="lazy"
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </CardMedia>                           
                            <CardActions disableSpacing>
                                <IconButton aria-label="ir a primera generación" href="/generacion/1">
                                <ArrowCircleRightIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardHeader
                                avatar={
                                    <Avatar>2da</Avatar>
                                }
                                title="Segunda Generación"
                                subheader="21 de noviembre de 1999"
                            />
                            <CardMedia>
                                <ImageList cols={3}>
                                    {imgSegundaGen.map((item) => (
                                        <ImageListItem key={item.img}>
                                            <img
                                                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                                alt={item.title}
                                                loading="lazy"
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </CardMedia>
                            <CardActions disableSpacing>
                                <IconButton aria-label="ir a segunda generación" href="/generacion/2">
                                <ArrowCircleRightIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardHeader
                                avatar={
                                    <Avatar>3ra</Avatar>
                                }
                                title="Tercera Generación"
                                subheader="27 de febrero de 1996"
                            />
                            <CardMedia>
                                <ImageList cols={3}>
                                    {imgTerceraGen.map((item) => (
                                        <ImageListItem key={item.img}>
                                            <img
                                                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                                alt={item.title}
                                                loading="lazy"
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </CardMedia>
                            <CardActions disableSpacing>
                                <IconButton aria-label="ir a tercera generación" href="/generacion/3">
                                <ArrowCircleRightIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardHeader
                                avatar={
                                    <Avatar>4ta</Avatar>
                                }
                                title="Cuarta Generación"
                                subheader="27 de febrero de 1996"
                            />
                            <CardMedia>
                                <ImageList cols={3}>
                                    {imgCuartaGen.map((item) => (
                                        <ImageListItem key={item.img}>
                                            <img
                                                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                                alt={item.title}
                                                loading="lazy"
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </CardMedia>
                            <CardActions disableSpacing>
                                <IconButton aria-label="ir a cuarta generación" href="/generacion/4">
                                <ArrowCircleRightIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardHeader
                                avatar={
                                    <Avatar>5ta</Avatar>
                                }
                                title="Quinta Generación"
                                subheader="27 de febrero de 1996"
                            />
                            <CardMedia>
                                <ImageList cols={3}>
                                    {imgQuintaGen.map((item) => (
                                        <ImageListItem key={item.img}>
                                            <img
                                                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                                alt={item.title}
                                                loading="lazy"
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </CardMedia>
                            <CardActions disableSpacing>
                                <IconButton aria-label="ir a quinta generación" href="/generacion/5">
                                <ArrowCircleRightIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardHeader
                                avatar={
                                    <Avatar>6ta</Avatar>
                                }
                                title="Sexta Generación"
                                subheader="27 de febrero de 1996"
                            />
                            <CardMedia>
                                <ImageList cols={3}>
                                    {imgSextaGen.map((item) => (
                                        <ImageListItem key={item.img}>
                                            <img
                                                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                                alt={item.title}
                                                loading="lazy"
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </CardMedia>
                            <CardActions disableSpacing>
                                <IconButton aria-label="ir a sexta generación" href="/generacion/6">
                                    <ArrowCircleRightIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item md={4}>
                        <Card>
                            <CardHeader
                                avatar={
                                    <Avatar>7ma</Avatar>
                                }
                                title="Séptima Generación"
                                subheader="27 de febrero de 1996"
                            />
                            <CardMedia>
                                <ImageList cols={3}>
                                    {imgSeptimaGen.map((item) => (
                                        <ImageListItem key={item.img}>
                                            <img
                                                src={`${item.img}?w=164&h=164&fit=crop&auto=format`}
                                                srcSet={`${item.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                                                alt={item.title}
                                                loading="lazy"
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </CardMedia>
                            <CardActions disableSpacing>
                                <IconButton aria-label="ir a sexta generación" href="/generacion/7">
                                    <ArrowCircleRightIcon />
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
                <div style={{ padding: 50 }}></div>
            </AppFrame>
        );
      }
}
const imgPrimeraGen = [
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png',
        title: 'Imagen1'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png',
        title: 'Imagen4'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png',
        title: 'Imagen7'
    },
];

const imgSegundaGen = [
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/152.png',
        title: 'Imagen152'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/155.png',
        title: 'Imagen155'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/158.png',
        title: 'Imagen158'
    },
];

const imgTerceraGen = [
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/252.png',
        title: 'Imagen252'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/255.png',
        title: 'Imagen255'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/258.png',
        title: 'Imagen258'
    },
];

const imgCuartaGen = [
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/387.png',
        title: 'Imagen387'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/390.png',
        title: 'Imagen390'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/393.png',
        title: 'Imagen393'
    },
];

const imgQuintaGen = [
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/495.png',
        title: 'Imagen495'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/498.png',
        title: 'Imagen498'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/501.png',
        title: 'Imagen501'
    },
];

const imgSextaGen = [
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/650.png',
        title: 'Imagen650'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/653.png',
        title: 'Imagen653'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/656.png',
        title: 'Imagen656'
    },
];
const imgSeptimaGen = [
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/722.png',
        title: 'Imagen387'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/725.png',
        title: 'Imagen390'
    },
    {
        img: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/728.png',
        title: 'Imagen393'
    },
];
MainPage.propTypes = {
    setBoolHome: PropTypes.func.isRequired,
    clearInput: PropTypes.func.isRequired,
  };
  
  const mapDispatchToProps = dispatch => ({
    setBoolHome: bool => dispatch(boolHome(bool)),
    clearInput: () => dispatch(cleanInput()),
  });
  
  export default connect(null, mapDispatchToProps)(MainPage);