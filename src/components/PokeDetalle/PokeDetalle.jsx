/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { boolHome, cleanInput, showDetails } from './../../actions/index';
import AppFrame from './../../components/AppFrame'
import './style.scss';

class PokeDetalle extends React.Component {
  constructor(props) {
    super(props);
    const { setBool, clearInput } = props;
    setBool(true);
    clearInput();
  }

  componentDidMount() {
    const { name } = this.props.match.params;
    const { details } = this.props;
    const pokemonUrl = `https://pokeapi.co/api/v2/pokemon/${name}`;
    console.log(pokemonUrl);
    const pokemonSpecies = `https://pokeapi.co/api/v2/pokemon-species/${name}`;
    axios.get(pokemonUrl)
      .then(data => {
        details(data, 1)
      });

    axios.get(pokemonSpecies)
      .then(data => {
        details(data, 2)
      });
  }

  bgColor(type) {
    switch (type) {
      case 'fire': {
        return '#FFA756';
      }
      case 'fairy': {
        return '#EBA8C3';
      }
      case 'grass': {
        return '#8BBE8A';
      }
      case 'water': {
        return '#58ABF6';
      }
      case 'bug': {
        return '#8BD674';
      }
      case 'normal': {
        return '#B5B9C4';
      }
      case 'poison': {
        return '#9F6E97';
      }
      case 'electric': {
        return '#F2CB55';
      }
      case 'ground': {
        return '#F78551';
      }
      case 'fighting': {
        return '#EB4971';
      }
      default: return '#ea5d60';
    }
  }


  render() {
    const { listDetails } = this.props
    const long = listDetails.entriesText.length;
    const pokemonImg = `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${listDetails.index}.png`;

    return (
      <AppFrame>
        <Card className="container">
        <CardHeader style={{ backgroundColor: this.bgColor(listDetails.color) }}
          avatar={
          <Avatar className="title">
              {listDetails.index}
          </Avatar>
          }
          title={listDetails.name
            .toLowerCase()
            .split(' ')
            .map(s => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' ')}
        />
        <CardContent>
          <Grid container xs={12}>
            <img src={pokemonImg} alt="" data-testid="img"/>
          </Grid>
          <Grid container xs={12}>
            "{listDetails.entriesText[Math.floor(Math.random() * long)]}"
          </Grid>
          <Grid container xs={12}>
            <h4 style={{ color: this.bgColor(listDetails.color) }}>Pokedex Data</h4>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="poke table">
              <TableHead>
                <TableRow>
                  <TableCell>Height</TableCell>
                  <TableCell>Weight</TableCell>
                  <TableCell>Types</TableCell>
                  <TableCell>Abilities</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableCell>{listDetails.height}</TableCell>
                <TableCell>{listDetails.weight}</TableCell>
                <TableCell>
                  {listDetails.type.map(type =>
                    <p data-testid="type" key={type.type.name}>{type.type.name}</p>
                  )}
                </TableCell>
                <TableCell>{listDetails.abilities.map(group => 
                    <p key={group.ability.name}>{group.ability.name}</p>)}
                </TableCell>
              </TableBody>
            </Table>
            </TableContainer>
          </Grid>
          <Grid item xs={12}>
            <h4 style={{ color: this.bgColor(listDetails.color) }}>Training</h4>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="poke table">
              <TableHead>
                <TableRow>
                  <TableCell>Capture rate</TableCell>
                  <TableCell>Base Friendship</TableCell>
                  <TableCell>Base Experience</TableCell>
                  <TableCell>Growth Rate</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableCell>{listDetails.captureRate}</TableCell>
                <TableCell>{listDetails.baseHappiness}</TableCell>
                <TableCell>{listDetails.baseExperience}</TableCell>
                <TableCell>{listDetails.growthRate}</TableCell>
              </TableBody>
            </Table>
            </TableContainer>
          </Grid>
          <Grid item xs={12}>
            <h4 style={{ color: this.bgColor(listDetails.color) }}>Breeding</h4>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="poke table">
              <TableHead>
                <TableRow>
                  <TableCell>Egg Groups</TableCell>
                  <TableCell>Gender Rate</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableCell>{listDetails.eggGroups.map(group => <p key={group.name}>{group.name}</p>)}</TableCell>
                <TableCell>{listDetails.genderRate}</TableCell>
              </TableBody>
            </Table>
            </TableContainer>
          </Grid>
        </CardContent>
        </Card>
      </AppFrame>
    );
  }
}

PokeDetalle.propTypes = {
  setBool: PropTypes.func.isRequired,
  clearInput: PropTypes.func.isRequired,
  listDetails: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => ({
  setBool: bool => dispatch(boolHome(bool)),
  clearInput: () => dispatch(cleanInput()),
  details: (detail, update) => dispatch(showDetails(detail, update))
});

const mapStateToProps = state => ({
  listDetails: state.details,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PokeDetalle));
