import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardMedia from '@material-ui/core/CardMedia'
import Avatar from '@material-ui/core/Avatar'
import { Button, CardActions } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  title: {
    background: '#F5F7F0'
  },
  pokemon_card: {
    maxWidth: 347,
    margin: 10
  }
}));

const Pokemon = props => {
  const classes = useStyles();
  const { name, url } = props;
  const index = url.split('/')[url.split('/').length - 2];
  const setIndex = (index.length === 3) ? index : (('0').repeat(3 - index.length) + index);
  const imageUrl = `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${setIndex}.png`;  
  return (
    <Card className={classes.pokemon_card}>
      <CardHeader className={classes.title}
          avatar={
          <Avatar>
              {setIndex}
          </Avatar>
          }
          title={name
            .toLowerCase()
            .split(' ')
            .map(s => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' ')}
      />
      <CardMedia data-testid="pokemon_img"
          component="img"
          height="250"
          image={imageUrl}
          alt=""
      />
      <CardActions>
          <Button color="primary" size="large" component={Link} to={`/pokemon/${name}`} data-testid="pokemon_name">Leer más</Button>
      </CardActions>
    </Card>      
  );
};

Pokemon.propTypes = {
  name: PropTypes.string,
  url: PropTypes.string,
};

Pokemon.defaultProps = {
  name: '',
  url: '',
};

export default withRouter(Pokemon);
