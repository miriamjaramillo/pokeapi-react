import React from 'react';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Avatar from '@material-ui/core/Avatar'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    header: {
        marginBottom: 20
    },
});

const Header = () => {
    const classes = useStyles();
    return(
        <div className={classes.header}>
            <AppBar position="static">
                <Toolbar>
                    <Avatar variant="square" alt="PokeAPI" src="https://raw.githubusercontent.com/PokeAPI/media/master/logo/pokeapi.svg?sanitize=true"
                    style={{width: 200, height: 80 }} />
                </Toolbar>
            </AppBar>
        </div>
    )
}
export default Header;